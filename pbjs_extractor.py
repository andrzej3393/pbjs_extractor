import click
import csv
import requests
from io import BytesIO, TextIOWrapper
from multiprocessing.pool import Pool
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, WebDriverException, JavascriptException
from tqdm import tqdm
from typing import Tuple, List
from urllib.parse import urlparse, urlunparse
from zipfile import ZipFile


def download_domains(url: str) -> List[str]:
    print("Downloading domains…")
    response = requests.get(url)
    buffer = BytesIO(response.content)
    with ZipFile(buffer, 'r') as unzipped_buffer:
        with unzipped_buffer.open('top-1m.csv', 'r') as csv_file_bytes:
            csv_file = TextIOWrapper(csv_file_bytes)
            reader = csv.reader(csv_file)
            urls = [row[1] for row in reader]
    print("Downloaded {} domains!".format(len(urls)))
    return urls


def make_url(domain: str) -> str:
    url_parts = urlparse(domain)
    if not url_parts.scheme:
        url_parts = ('http', domain, '', '', '', '')
    return urlunparse(url_parts)


def extract_single_page(domain: str) -> Tuple[str, str]:
    url = make_url(domain)

    try:
        code = requests.get(url, timeout=30).status_code
    except requests.exceptions.Timeout:
        return domain, "took too much time to load"
    except requests.exceptions.ConnectionError:
        return domain, "failed to resolve domain name"
    except requests.exceptions.RequestException:
        return domain, "unrecognized problem with site"

    if code != 200:
        return domain, f"site response code: {code}"

    options = webdriver.ChromeOptions()
    options.headless = True
    driver = webdriver.Chrome(options=options)
    driver.set_page_load_timeout(30)
    try:
        driver.get(url)
        result = driver.execute_script("return JSON.stringify(pbjs)")
    except JavascriptException:
        result = "pbjs not found"
    except TimeoutException:
        result = "took too much time to load"
    except WebDriverException as e:
        result = e.msg.split('\n')[0]
    driver.close()
    return domain, result


@click.command()
@click.option(
    '--domains-url',
    default='http://s3.amazonaws.com/alexa-static/top-1m.csv.zip',
    help='URL where script will look for file with domains',
)
@click.option(
    '--result-filename',
    default='out.csv',
    help='Name of file which will contain output data',
)
def main(domains_url, result_filename):
    domains = download_domains(url=domains_url)
    with open(result_filename, 'w', newline='') as f:
        writer = csv.writer(f)
        with tqdm(total=len(domains)) as pbar:
            with Pool(10) as p:
                for result in p.imap(extract_single_page, domains, chunksize=10):
                    writer.writerow(result)
                    pbar.update(1)


if __name__ == '__main__':
    main()
