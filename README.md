pbjs_extractor
==============

Simple script which downloads zip archive with list of domains from which it extracts `pbjs` data and saves it to the CSV file.

Technical details
-----------------
At first, script downloads list of domains to check. Downloaded ZIP is handled in memory, where it's unzipped and CSV that's inside it is being processed into list of domains.

Then, output CSV is opened and in it's context `pbjs` data are extracted from sites. Extraction is run in parallel (multiprocess) and output data from each process is saved to the aforemntioned CSV file.

Script extracts data with average speed around 1000 sites per 20 minutes on common business machine.

How to use it?
--------------

1. You'll need Python version 3.6 or newer.
2. You'll also need to setup chromedriver for selenium ([docs](https://selenium-python.readthedocs.io/installation.html#drivers)).
3. Then you have to install few required Python packages: `pip install Click==7.0 requests==2.21 selenium==3.141 tqdm==4.28`.
4. Now you can run the script: `python pbjs_extractor.py`

Script also can accept optional parameters:
 * `--domains-url TEXT`
 * `--result-filename TEXT` 

If you want to know more about these parameters, just run the script with `--help` parameter.
